$(document).ready(function() {
CucumberHTML.timelineItems.pushArray([
  {
    "id": "as-a-customer--i-want-to-select-the-type-of-car-i-want-to-sell--so-that-i-can-receive-an-offer-from-auto1.com.;to-verify-the-manufacturer-drop-down-rest-service",
    "feature": "As a customer, I want to select the type of car I want to sell, so that I can receive an offer from auto1.com.",
    "scenario": "To verify the manufacturer drop down REST Service",
    "start": 1595072818618,
    "end": 1595072823106,
    "group": 1,
    "content": "",
    "className": "passed",
    "tags": "@basicfeatures,"
  },
  {
    "id": "as-a-customer--i-want-to-select-the-type-of-car-i-want-to-sell--so-that-i-can-receive-an-offer-from-auto1.com.;to-verify-the-car-build-dates-drop-down-rest-service",
    "feature": "As a customer, I want to select the type of car I want to sell, so that I can receive an offer from auto1.com.",
    "scenario": "To verify the car build dates drop down Rest Service",
    "start": 1595072823404,
    "end": 1595072823610,
    "group": 1,
    "content": "",
    "className": "passed",
    "tags": "@basicfeatures,"
  },
  {
    "id": "as-a-customer--i-want-to-select-the-type-of-car-i-want-to-sell--so-that-i-can-receive-an-offer-from-auto1.com.;to-verify-the-main-type-drop-down-list-rest-service",
    "feature": "As a customer, I want to select the type of car I want to sell, so that I can receive an offer from auto1.com.",
    "scenario": "To verify the main type drop down list REST Service",
    "start": 1595072823111,
    "end": 1595072823398,
    "group": 1,
    "content": "",
    "className": "passed",
    "tags": "@basicfeatures,"
  }
]);
CucumberHTML.timelineGroups.pushArray([
  {
    "id": 1,
    "content": "Thread[main,5,main]"
  }
]);
});