$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/features/APITest.feature");
formatter.feature({
  "name": "As a customer, I want to select the type of car I want to sell, so that I can receive an offer from auto1.com.",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@BasicFeatures"
    }
  ]
});
formatter.scenario({
  "name": "To verify the manufacturer drop down REST Service",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BasicFeatures"
    }
  ]
});
formatter.step({
  "name": "I want to execute getManufactures list endpoint",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.java:39"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I submit the GET request",
  "keyword": "When "
});
formatter.match({
  "location": "MyStepdefs.java:43"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should get 200 success status code",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepdefs.java:47"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "To verify the main type drop down list REST Service",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BasicFeatures"
    }
  ]
});
formatter.step({
  "name": "I want to execute getMainTypes list endpoint",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.java:55"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I submit the GET request with manufacture code",
  "keyword": "When "
});
formatter.match({
  "location": "MyStepdefs.java:58"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should get the response associated with manufacture\u0027s main types and 200 success status code",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepdefs.java:64"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "To verify the car build dates drop down Rest Service",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BasicFeatures"
    }
  ]
});
formatter.step({
  "name": "I want to execute getBuildDates list endpoint",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.java:72"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I submit the GET request with Manufacture and Main type details",
  "keyword": "When "
});
formatter.match({
  "location": "MyStepdefs.java:75"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should get the response with list of build dates and 200 success status code",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepdefs.java:80"
});
formatter.result({
  "status": "passed"
});
});