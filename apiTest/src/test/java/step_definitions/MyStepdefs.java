package step_definitions;

import commonFunctions.CommonFuns;
import cucumber.api.java8.En;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Properties;

public class MyStepdefs extends CommonFuns implements En {
    private String url;
    private String manufacturerPath;
    private String mainTypesPath;
    private String buildDatesPath;
    private String paramKey;
    private String paramValue;
    private Response response;
    private LinkedHashMap<String,String> getAllWkdaData = new LinkedHashMap<>();

    public MyStepdefs() throws IOException {


        FileReader reader=new FileReader("src/main/java/readData.properties");
        Properties read=new Properties();
        read.load(reader);
        url=read.getProperty("baseUrl");
        manufacturerPath=read.getProperty("basePathManufacturer");
        mainTypesPath=read.getProperty("basePathMainTypes");
        buildDatesPath=read.getProperty("basePathBuildDates");
        paramKey=read.getProperty("key");
        paramValue=read.getProperty("value");
        RestAssured.baseURI= url;


        Given("^I want to execute getManufactures list endpoint$", () -> {

            RestAssured.basePath=manufacturerPath;
        });
        When("^I submit the GET request$", () -> {
            RequestSpecification request=RestAssured.given();
             response =request.queryParam(paramKey,paramValue).get();
        });
        Then("^I should get (\\d+) success status code$", (Integer arg0) -> {
            Assert.assertEquals( response.getStatusCode(),200);
            getAllWkdaData =response.path("wkda");
            Assert.assertTrue(getAllWkdaData.containsKey("285"));
            getWkdaList(getAllWkdaData);
        });


        Given("^I want to execute getMainTypes list endpoint$", () -> {
            RestAssured.basePath=mainTypesPath;
        });
        When("^I submit the GET request with manufacture code$", () -> {
            RequestSpecification requestSpecification=RestAssured.given();
            response=requestSpecification.queryParam(paramKey,paramValue).queryParam("manufacturer","285").get();

        });
        Then("^I should get the response associated with manufacture's main types and (\\d+) success status code$", (Integer arg0) -> {
            getAllWkdaData =response.jsonPath().get("wkda");
            Assert.assertEquals( response.getStatusCode(),200);
            Assert.assertTrue(getAllWkdaData.containsKey("Galaxy"));
            getWkdaList(getAllWkdaData);
        });


        Given("^I want to execute getBuildDates list endpoint$", () -> {
            RestAssured.basePath=buildDatesPath;
        });
        When("^I submit the GET request with Manufacture and Main type details$", () -> {
            RequestSpecification requestSpecification=RestAssured.given();
            response=requestSpecification.queryParam(paramKey,paramValue).queryParam("manufacturer","285")
            .queryParam("main-type","Galaxy").get();
        });
        Then("^I should get the response with list of build dates and (\\d+) success status code$", (Integer arg0) -> {
            getAllWkdaData=response.jsonPath().get("wkda");
            Assert.assertEquals(response.getStatusCode(),200);
            Assert.assertTrue(getAllWkdaData.containsKey("2019"));
            getWkdaList(getAllWkdaData);
        });
    }
}
