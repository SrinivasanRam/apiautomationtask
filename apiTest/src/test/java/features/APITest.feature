@BasicFeatures
Feature: As a customer, I want to select the type of car I want to sell, so that I can receive an offer from auto1.com.

  Scenario: To verify the manufacturer drop down REST Service
    Given I want to execute getManufactures list endpoint
    When I submit the GET request
    Then I should get 200 success status code

  Scenario: To verify the main type drop down list REST Service
    Given I want to execute getMainTypes list endpoint
    When I submit the GET request with manufacture code
    Then I should get the response associated with manufacture's main types and 200 success status code

  Scenario: To verify the car build dates drop down Rest Service
    Given I want to execute getBuildDates list endpoint
    When I submit the GET request with Manufacture and Main type details
    Then I should get the response with list of build dates and 200 success status code