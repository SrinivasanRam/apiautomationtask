package testCases;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import commonFunctions.CommonFuns;
import org.testng.annotations.Test;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class RestClass extends CommonFuns {
    public String url;
    public String manufacturerPath;
    public String mainTypesPath;
    public String buildDatesPath;
    public String paramKey;
    public String paramValue;




    public RestClass() throws IOException {
        FileReader reader=new FileReader("src/main/java/testDataFolder/readData.properties");
        Properties read=new Properties();
        read.load(reader);
        url = read.getProperty("baseUrl");
        manufacturerPath = read.getProperty("basePathManufacturer");
        mainTypesPath = read.getProperty("basePathMainTypes");
        buildDatesPath = read.getProperty("basePathBuildDates");
        paramKey = read.getProperty("key");
        paramValue = read.getProperty("value");
    }



    @Test()
    public void getManufacturerList() {
         getCarDetailsLists(paramKey, paramValue, url, manufacturerPath,"","","","");

    }

   @Test
    public void getMainTypeLists( ) {
    List getManufacturerList = getCarDetailsLists(paramKey, paramValue, url, manufacturerPath,"","","","");
    for (int j = 0; j <= getManufacturerList.size() - 1; j++) {
        given().param(paramKey, paramValue)
                .param("manufacturer", getManufacturerList.get(j).toString())
                .when().get(url + mainTypesPath).then().statusCode(200).extract().response().prettyPrint();

    }
}
    @Test
    public void getBuildDatesLists(){
        List getManufacturerList = getCarDetailsLists(paramKey, paramValue, url, manufacturerPath,"","","","");

        for (int j = 0; j <= getManufacturerList.size() - 1; j++) {
            List getMainTypesLists = getCarDetailsLists(paramKey, paramValue, url,mainTypesPath,"manufacturer",getManufacturerList.get(j).toString(),"","");
            for(int k=0;k<=getMainTypesLists.size()-1;k++){
                List getBuildDates= getCarDetailsLists(paramKey, paramValue, url,buildDatesPath,
                        "manufacturer",getManufacturerList.get(j).toString(),
                        "main-type",getMainTypesLists.get(k).toString());

                System.out.println(getManufacturerList.get(j).toString()+"="+getMainTypesLists.get(k).toString()+"="+getBuildDates);
            }


        }


    }
}
