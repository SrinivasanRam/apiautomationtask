package commonFunctions;
import static io.restassured.RestAssured.*;
import org.testng.Assert;
import java.util.*;


public class CommonFuns  {

   protected List getCarDetailsLists(String paramKey, String paramValue, String url, String manufacturerPath, String paramKey1, String paramValue1, String paramkey2, String paramValue2){
       LinkedHashMap<String,String> manufacturerLists = given().param(paramKey, paramValue).param(paramKey1,paramValue1).param(paramkey2,paramValue2)
        .when().get(url + manufacturerPath).then().statusCode(200).extract().path("wkda");
        Set<String>keyTaken=manufacturerLists.keySet();
        List array = new ArrayList(keyTaken);
        if(array==null){
        Assert.fail("No key or data found under wkda");
        }
       return array;
   }


    protected List getWkdaList(LinkedHashMap<String, String> json){

        Set<String>keyTaken=json.keySet();
        List array = new ArrayList(keyTaken);
        if(array==null){
            Assert.fail("No key or data found under wkda");
        }
        return array;

    }


}
