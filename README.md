# APIAutomationTask

To automate the API services I have used REST Assured framework.

- Programming Language : java version "1.8.0_231"
- All automation testing related libraries are available in pom.xml.For example TestNg,RestAssured and Cucumber for BDD.

- I have created 2 ways of  behaviour driven development code, 
- one with cucumber BDD and another with In-build Rest Assured given when then concept


- Test Report: Cucumber report and testNg report is been used
- Test Data : All data will be retrieved from readData.properties file

**How to run the script:**

Two separate scripts created with BDD,


1.  Go to src -> test -> testCases - >RestClass.java (The syntax of Rest Assured.io is the most beautiful part, as it is very BDD like and understandable.)
2.  Go to src -> test -> testCases - >Runner.java

**Note:** 
- I would recommend to use rest assured BDD to develop API automation.
- Since URL and wkda code is confidential, I am removed those from property file.

``

